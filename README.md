# Responsible Type #

The purpose of this project to create a checklist for designers when designing for type on the web. So far this project contains a SASS with lots of hocks.

The list so far includes;
* Typefaces
* Font styles & weights
* Two scales (one the Golden Ratio and the other Pamental Scale)
* Letter spacing / kerning / tracking
* Line height / Leading
* Ligatures
* Legibility
* Justification
* Indentation
* Overflow
* Characters per line
* Decoration
* Effects
* Rotation
* Case